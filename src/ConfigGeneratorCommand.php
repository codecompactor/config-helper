<?php

namespace Codecompactor\ConfigHelper;


use Composer\Factory as ComposerFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class ConfigGeneratorCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('config:generate')
            ->setDescription('Helps configure your projects CodeCompactor configuration');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $composerFile = ComposerFactory::getComposerFile();
        $config = json_decode(file_get_contents($composerFile), true);

        if (!array_key_exists('extra', $config)) {
            $config['extra'] = [];
        }

        if (!array_key_exists('codecompactor', $config['extra'])) {
            $config['extra']['codecompactor'] = [];
        }

        $cc_config = &$config['extra']['codecompactor'];

        $access_token = array_key_exists('access_token', $cc_config) ? $cc_config['access_token'] : 'TBD';
        $root_path = array_key_exists('root_path', $cc_config) ? $cc_config['root_path'] : './';
        $setup_scripts = array_key_exists('setup_scripts', $cc_config) ? $cc_config['setup_scripts'] : [];
        $ignored_files = array_key_exists('ignored_files', $cc_config) ? $cc_config['ignored_files'] : [];

        $output->write(PHP_EOL);

        $output->writeln('This command will help insert your CodeCompactor config into your composer.json file.');
        $output->writeln('Leave fields blank to keep the current value.');

        $output->write(PHP_EOL);

        $access_token = $this->ask($input, $output, 'What is this repositories access token?', $access_token);
        $root_path = $this->ask($input, $output, 'What is the root path of your project?', $root_path);

        $output->writeln("You have ".count($setup_scripts)." setup scripts");
        if ($this->confirm($input, $output, 'Manage them?', false)) {
            $new_scripts = [];
            foreach ($setup_scripts as $setup_script) {
                $choice = $this->choice(
                    $input,
                    $output,
                    $setup_script,
                    [
                        'Keep',
                        'Remove',
                        'Edit'
                    ]
                );
                switch ($choice) {
                    case 'Keep':
                        $new_scripts[] = $setup_script;
                        break;
                    case 'Remove':
                        $output->writeln('Script removed');
                        break;
                    case 'Edit':
                        $new_scripts[] = $this->ask($input, $output, 'Input new script:', $setup_script);
                }
            }
            $setup_scripts = $new_scripts;

            while ($newScript = $this->ask($input, $output, 'Additional script: (leave blank to skip)')) {
                $setup_scripts[] = $newScript;
                $output->writeln('Script added');
                $output->write(PHP_EOL);
            }
        }

        $output->writeln("You have ".count($ignored_files)." ignored files");
        if ($this->confirm($input, $output, 'Manage them?', false)) {
            $new_ignored_files = [];
            foreach ($ignored_files as $ignored_file) {
                $choice = $this->choice(
                    $input,
                    $output,
                    $ignored_file,
                    [
                        'Keep',
                        'Remove',
                        'Edit'
                    ]
                );
                switch ($choice) {
                    case 'Keep':
                        $new_ignored_files[] = $ignored_file;
                        break;
                    case 'Remove':
                        $output->writeln('Ignored file no longer ignored');
                        break;
                    case 'Edit':
                        $new_ignored_files[] = $this->ask($input, $output, 'Input new ignored file:', $ignored_file);
                }
            }
            $ignored_files = $new_ignored_files;

            while ($newIgnoredFile = $this->ask($input, $output, 'Additional ignored filename: (leave blank to skip)')) {
                $ignored_files[] = $newIgnoredFile;
                $output->writeln('File added to ignore list');
                $output->write(PHP_EOL);
            }
        }

        $cc_config['access_token'] = $access_token;
        $cc_config['root_path'] = $root_path;
        $cc_config['setup_scripts'] = $setup_scripts;
        $cc_config['ignored_files'] = $ignored_files;

        $newJson = json_encode($cc_config, JSON_PRETTY_PRINT);
        $output->writeln($newJson);
        if ($this->confirm($input, $output, 'Confirm generation?', false)) {
            file_put_contents($composerFile, json_encode($config, JSON_PRETTY_PRINT));
            $output->writeln('Composer file updated');
        }else {
            $output->writeln('No action taken');
        }

    }

    private function ask(InputInterface $input, OutputInterface $output, string $questionToAsk, string $default = '') {
        if ($default) {
            $questionToAsk .= PHP_EOL . "currently '{$default}'";
        }

        $response = $this
            ->getHelper('question')
            ->ask(
                $input,
                $output,
                new Question($questionToAsk.PHP_EOL, $default)
            );

        $output->write(PHP_EOL);

        return $response;
    }

    private function choice(InputInterface $input, OutputInterface $output, string $questionToAsk, array $choices) {
        $response = $this
            ->getHelper('question')
            ->ask(
                $input,
                $output,
                new ChoiceQuestion(
                    $questionToAsk,
                    $choices
                )
            );
        return $response;
    }

    private function confirm(InputInterface $input, OutputInterface $output, string $questionToAsk, bool $default) {
        $questionToAsk .= ' (' . ($default ? 'Yes' : 'No') . ')' . PHP_EOL;

        $response = $this
            ->getHelper('question')
            ->ask(
                $input,
                $output,
                new ConfirmationQuestion($questionToAsk, $default)
            );

        $output->write(PHP_EOL);

        return $response;
    }

}