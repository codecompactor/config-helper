<?php

namespace Codecompactor\ConfigHelper;

use Composer\Factory as ComposerFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ConfigValidatorCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('config:validate')
            ->setDescription('Validates your CodeCompactor config');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $composerFile = ComposerFactory::getComposerFile();

        $baseConfig = json_decode(file_get_contents($composerFile), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            self::fail($output, [
                'Your composer.json file is not valid json.',
                'error message: '.json_last_error_msg()
            ]);
            return;
        }

        if (!array_key_exists('extra', $baseConfig) || !array_key_exists('codecompactor', $baseConfig['extra'])) {
            self::fail($output, [
                'It looks like you have not yet ran the CodeCompactor config generator script',
                'Please run: <info>php vendor/bin/codecompactor config:generate</info>'
            ]);
            return;
        }

        $ccConfig = $baseConfig['extra']['codecompactor'];

        $baseConfigOptions = [
            'name',
            'version',
            'description',
        ];
        $ccConfigOptions = [
            'access_token',
            'root_path',
        ];

        foreach ($baseConfigOptions as $option) {
            if (!array_key_exists($option, $baseConfig)) {
                self::fail($output, "You are missing the <info>{$option}</info> root-level item in your composer.json file.");
                return;
            }
        }

        foreach ($ccConfigOptions as $option) {
            if (!array_key_exists($option, $ccConfig)) {
                self::fail($output, [
                    "You are missing the <info>{$option}</info> item in your CodeCompactor configuration.",
                    'Please run: <info>php vendor/bin/codecompactor config:generate</info>'
                ]);
                return;
            }
        }

        $output->writeln('Validation passed');

    }

    private static function fail(OutputInterface $output, $reason) {
        $output->writeln('<error>Validation Failed</error>');
        $output->write(PHP_EOL);
        $output->writeln($reason);
    }

}