CodeCompactor Configuration Helper
==================================
This package helps you generate the configuration for the CodeCompactor
build service.

Installation
------------
Installation is fairly straightforward with composer.

`composer require --dev codecompactor/config-helper`

It is recommended that this project not be used in production.

Usage
-----
This package will walk you through step-by-step the process of
setting up your configuration. Simply type this into your terminal:

`./vendor/bin/codecompactor config:generate`

All that you need to do now is follow the instructions.

Obtaining Access Token
----------------------
Although you do not need an access token to use this package, you
will need one to use the CodeCompactor service. Head to codecompactor.com
to setup an account.